package main

import (
	"bytes"
	"fmt"
	"image/color"
	"log"
	"math"
	"math/rand"
	"net/http"
	"time"

	"golang.org/x/net/websocket"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgsvg"
)

const page = `
<html>
	<head>
		<title>GCM Temperature monitoring</title>
		<script type="text/javascript">
		var sock = null;
		var UpperLeft = "";
		var UpperRight = "";
		var Bottom = "";
		var FPGA_A = "";
		var FPGA_B = "";
		var LTM_U107 = "";
		var LTM_U108 = "";
		var LTM_U109 = "";
		var LTM_U110 = "";
		var LTM_U6 = "";
		var LTM_U7 = "";
		var LTM_U8 = "";

		function update() {
			var p1 = document.getElementById("UpperLeft");
			p1.innerHTML = UpperLeft;

			var p2 = document.getElementById("UpperRight");
			p2.innerHTML = UpperRight;
			var p3 = document.getElementById("Bottom");
			p3.innerHTML = Bottom;
			var p4 = document.getElementById("FPGA_A");
			p4.innerHTML = FPGA_A;
			var p5 = document.getElementById("FPGA_B");
			p5.innerHTML = FPGA_B;
			var p6 = document.getElementById("LTM_U107");
			p6.innerHTML = LTM_U107;
			var p8 = document.getElementById("LTM_U108");
			p8.innerHTML = LTM_U108;
			var p9 = document.getElementById("LTM_U109");
			p9.innerHTML = LTM_U109;
			var p10 = document.getElementById("LTM_U110");
			p10.innerHTML = LTM_U110;
			var p11 = document.getElementById("LTM_U6");
			p11.innerHTML = LTM_U6;
			var p12 = document.getElementById("LTM_U7");
			p12.innerHTML = LTM_U7;
			var p13 = document.getElementById("LTM_U8");
			p13.innerHTML = LTM_U8;
			};

		window.onload = function() {
			sock = new WebSocket("ws://"+location.host+"/data");

			sock.onmessage = function(event) {
				var data = JSON.parse(event.data);
				//console.log("data: "+JSON.stringify(data));
				UpperLeft = data["Upleft board Temperature"];
				UpperRight = data["Upright board Temperature"];
				Bottom = data["Bottom board Temperature"];
				FPGA_A = data["FPGA A Temperature"];
				FPGA_B = data["FPGA B Temperature"];
				LTM_U107 = data["LTM4700 U107"]
				LTM_U108 = data["LTM4700 U108"]
				LTM_U109 = data["LTM4700 U109"]
				LTM_U110 = data["LTM4700 U110"]
				LTM_U6 = data["LTM4700 U6"]
				LTM_U7 = data["LTM4700 U7"]
				LTM_U8 = data["LTM4700 U8"]

				update();
			};
		};

		</script>

		<style>
		.my-plot-style {
			width: 1200px;
			height: 170px;
			font-size: 14px;
			line-height: 1.2em;
		}
		</style>
	</head>

	<body>
		<div id="header">
			<h2>GCM Temperatures</h2>
		</div>

		<div id="content">
			<div id="UpperLeft" class="my-plot-style"></div>
			<br>
			<div id="UpperRight" class="my-plot-style"></div>
			<br>
			<div id="Bottom" class="my-plot-style"></div>
			<br>
			<div id="FPGA_A" class="my-plot-style"></div>
			<br>
			<div id="FPGA_B" class="my-plot-style"></div>
			<br>
			<div id="LTM_U107" class="my-plot-style"></div>
			<br>
			<div id="LTM_U108" class="my-plot-style"></div>
			<br>
			<div id="LTM_U109" class="my-plot-style"></div>
			<br>
			<div id="LTM_U110" class="my-plot-style"></div>
			<br>
			<div id="LTM_U6" class="my-plot-style"></div>
			<br>
			<div id="LTM_U7" class="my-plot-style"></div>
			<br>
			<div id="LTM_U8" class="my-plot-style"></div>
		</div>
	</body>
</html>
`

var (
	datac   = make(chan map[string]string)
	done    = make(chan bool)
	running = false
)

func (ctrl *I2CController) plotHandle(w http.ResponseWriter, r *http.Request) {
	if running {
		done <- true
	}
	go ctrl.ReadOutLoop(datac, done)
	running = true
	fmt.Fprintf(w, page)
}

func dataHandler(ws *websocket.Conn) {
	for data := range datac {
		err := websocket.JSON.Send(ws, data)
		if err != nil {
			log.Printf("error sending data: %v\n", err)
			return
		}
	}
}

func renderSVG(p *plot.Plot) string {
	size := 10 * vg.Centimeter
	canvas := vgsvg.New(3*size, 0.8*size/vg.Length(math.Phi))
	p.Draw(draw.New(canvas))
	out := new(bytes.Buffer)
	_, err := canvas.WriteTo(out)
	if err != nil {
		panic(err)
	}
	return string(out.Bytes())
}

func (ctrl *I2CController) ReadOutLoop(datac chan map[string]string, done chan bool) {
	ticker := time.NewTicker(350 * time.Millisecond)
	defer ticker.Stop()

	results := ctrl.GetTemperatures()
	table := make(map[string]plotter.XYs, len(results))
	svgs := make(map[string]string, len(results))

	now := 0
	for {
		select {
		case <-ticker.C:
			now = int(time.Now().UTC().UnixMilli())
			results = ctrl.GetTemperatures()
			for i, res := range results {
				table[i] = append(table[i], struct{ X, Y float64 }{float64(now), float64(res)})
				svgs[i] = plotTemperature(table[i], i)
			}

			datac <- svgs

		case <-done:
			return
		}
	}
}

func plotTemperature(data plotter.XYs, plotname string) string {
	var err error = nil

	sin := plot.New()
	if err != nil {
		panic(err)
	}
	sin.X.Label.Text = "Time"
	sin.Y.Label.Text = "Temperature (C)"
	sin.Title.Text = plotname
	sin.X.Tick.Marker = commaTicks{}
	line, err := plotter.NewLine(data)
	if err != nil {
		panic(err)
	}
	line.LineStyle.Color = color.RGBA{255, 0, 0, 255}
	line.LineStyle.Width = vg.Points(1)

	sin.Add(line)
	sin.Add(plotter.NewGrid())

	return renderSVG(sin)
}

func (ctrl *I2CController) GetTemperatures() map[string]float32 {

	results := make(map[string]float32)

	for name := range ctrl.LTMs {
		var data []byte = []byte{0, 0}
		var err error
		if !ctrl.dryrun {
			//ctrl.Write(name)
			data, err = ctrl.WriteRead(name, []byte{0x8D}, 2)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			data = []byte{byte(rand.Intn(255)), byte(rand.Intn(255))}

		}
		value := uint32(uint16(data[1])<<8 + uint16(data[0]))
		temperature := lin5_11ToFloat(value)
		log.Printf("%v : %v C", name, temperature)
		results[name] = float32(temperature)
	}

	for name := range ctrl.TMP435s {
		var data []byte
		if !ctrl.dryrun {
			data, _ = ctrl.WriteRead(name, []byte{0x0}, 1)
		} else {
			data = []byte{byte(rand.Intn(255))}
		}

		value := uint16(data[0])
		log.Printf("%v : %v C", name, value)
		results[name] = float32(value)
	}

	for name := range ctrl.FPGAs {
		var data []byte
		if !ctrl.dryrun {
			data, _ = ctrl.WriteRead(name, []byte{0x0, 0x0, 0x0, 0x04}, 2)
		} else {
			data = []byte{byte(rand.Intn(255)), byte(rand.Intn(255))}
		}
		value := float32(uint16(data[1])<<8 + uint16(data[0]))
		//value = value>>8 | (value << 8 & 0xFF00)
		temperature := value*509.3140064/65536. - 280.23
		log.Printf("%v : %v C", name, temperature)
		results[name] = temperature
	}

	return results
}

type commaTicks struct{}

// Ticks computes the default tick marks, but inserts commas
// into the labels for the major tick marks.
func (commaTicks) Ticks(min, max float64) []plot.Tick {
	tks := plot.DefaultTicks{}.Ticks(min, max)
	for i, t := range tks {
		if t.Label == "" { // Skip minor ticks, they are fine.
			continue
		}
		tks[i].Label = time.UnixMilli(int64(t.Value)).String()
	}
	return tks
}
