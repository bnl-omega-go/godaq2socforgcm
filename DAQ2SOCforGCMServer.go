package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math"
	"math/rand"
	"net/http"
	"time"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
	"golang.org/x/net/websocket"
)

// Z_IIC_BUS2 COMPONENTS ADDRESS Definitions
const INA226_U132_ADDR = 0x40 // 1000_000X  0X80 INA226 12P0V Voltage/Current
const INA226_U133_ADDR = 0x41 // 1000_001X  0X82 INA226 VCCINT_A_0P72V Voltage/Current
const INA226_U134_ADDR = 0x44 // 1000_100X  0X88 INA226 MGTYAVCC_A_0P9V Voltage/Current
const INA226_U135_ADDR = 0x45 // 1000_101X  0X8A INA226 MGTYAVTT_A_1P2V Voltage/Current

// Z_IIC_BUS3 COMPONENTS ADDRESS Definitions
const INA226_U136_ADDR = 0x40 // 1000_000X  0X80 INA226 VCCINT_B_0P72V Voltage/Current
const INA226_U137_ADDR = 0x41 // 1000_001X  0X82 INA226 MGTYAVCC_B_0P9V Voltage/Current
const INA226_U138_ADDR = 0x44 // 1000_100X  0X88 INA226 MGTYAVTT_B_1P2V Voltage/Current
const INA226_U139_ADDR = 0x45 // 1000_101X  0X8A INA226 VCCINT_Z_IO_AB_0P85V Voltage/Current

const TMP435_U161_ADDR = 0x4C // 1001_100X  0X98 Temperature of FPGA A on die and board area
const TMP435_U162_ADDR = 0x4D // 1001_101X  0X9A Temperature of FPGA B on die and board area
const TMP435_U163_ADDR = 0x4E // 1001_110X  0X9C Temperature of FPGA Z on die and board area

// Z_IIC_BUS4 COMPONENTS ADDRESS Definitions
const INA226_U140_ADDR = 0x40 // 1000_000X  0X80 INA226 MGTAVCC_Z_0P9V Voltage/Current
const INA226_U141_ADDR = 0x41 // 1000_001X  0X82 INA226 MGTAVTT_Z_1P2V Voltage/Current
const INA226_U142_ADDR = 0x44 // 1000_100X  0X88 INA226 1P8V Voltage/Current
const INA226_U143_ADDR = 0x45 // 1000_101X  0X8A INA226 2P5V Voltage/Current

// Z_IIC_BUS5 COMPONENTS ADDRESS Definitions
const INA226_U146_ADDR = 0x40 // 1000_000X  0X80 INA226 DDR4_VDDQ_1P2V Voltage/Current
const INA226_U144_ADDR = 0x41 // 1000_001X  0X82 INA226 3P3V Voltage/Current
const INA226_U145_ADDR = 0x44 // 1000_100X  0X88 INA226 GRM 12P0V Voltage/Current

type Device struct {
	address uint
	bus     string
}

type I2CController struct {
	SlowControl.I2CController
	INA226s, TMP435s, FPGAs, LTMs map[string]Device
	dryrun                        bool
}

type GCMThreadController struct {
	stop chan bool
}

func main() {

	dryrun := flag.Bool("dry", false, "Dry run reading CSV file")
	flag.Parse()

	INA226_GCM := map[string]Device{
		"12p0V":                {INA226_U132_ADDR, "/dev/i2c-1"},
		"VCCINT_A_0P72V":       {INA226_U133_ADDR, "/dev/i2c-1"},
		"MGTYAVCC_A_0P9V":      {INA226_U134_ADDR, "/dev/i2c-1"},
		"MGTYAVTT_A_1P2V":      {INA226_U135_ADDR, "/dev/i2c-1"},
		"VCCINT_B_0P72V":       {INA226_U136_ADDR, "/dev/i2c-2"},
		"MGTYAVCC_B_0P9V":      {INA226_U137_ADDR, "/dev/i2c-2"},
		"MGTYAVTT_B_1P2V":      {INA226_U138_ADDR, "/dev/i2c-2"},
		"VCCINT_Z_IO_AB_0P85V": {INA226_U139_ADDR, "/dev/i2c-2"},
		"MGTAVCC_Z_0P9V":       {INA226_U140_ADDR, "/dev/i2c-3"},
		"MGTAVTT_Z_1P2V":       {INA226_U141_ADDR, "/dev/i2c-3"},
		"1P8V":                 {INA226_U142_ADDR, "/dev/i2c-3"},
		"2P5V":                 {INA226_U143_ADDR, "/dev/i2c-3"},
		"DDR4_VDDQ_1P2V":       {INA226_U144_ADDR, "/dev/i2c-4"},
		"3P3V":                 {INA226_U145_ADDR, "/dev/i2c-4"},
		"12P0V":                {INA226_U146_ADDR, "/dev/i2c-4"},
	}

	TMP435s := map[string]Device{
		"Upleft board Temperature":  {TMP435_U161_ADDR, "/dev/i2c-4"},
		"Upright board Temperature": {TMP435_U162_ADDR, "/dev/i2c-4"},
		"Bottom board Temperature":  {TMP435_U163_ADDR, "/dev/i2c-4"},
	}

	FPGA_TSensors := map[string]Device{
		"FPGA A Temperature": {0x32, "/dev/i2c-2"},
		"FPGA B Temperature": {0x32, "/dev/i2c-4"},
	}

	LTM4700_TSensors := map[string]Device{
		"LTM4700 U107": {0x40, "/dev/i2c-2"},
		"LTM4700 U108": {0x41, "/dev/i2c-2"},
		"LTM4700 U109": {0x42, "/dev/i2c-2"},
		"LTM4700 U110": {0x43, "/dev/i2c-2"},
		"LTM4700 U184": {0x44, "/dev/i2c-2"},
		"LTM4700 U6":   {0x45, "/dev/i2c-2"},
		"LTM4700 U7":   {0x46, "/dev/i2c-2"},
		"LTM4700 U8":   {0x48, "/dev/i2c-2"},
	}

	i2cctrl := I2CController{dryrun: *dryrun, INA226s: INA226_GCM, TMP435s: TMP435s, FPGAs: FPGA_TSensors, LTMs: LTM4700_TSensors}

	if !*dryrun {
		i2cctrl.Init()

		for key, dev := range INA226_GCM {
			i2cctrl.AddDevice(key, uint16(dev.address), dev.bus)
		}
		for key, dev := range TMP435s {
			i2cctrl.AddDevice(key, uint16(dev.address), dev.bus)
		}
		for key, dev := range FPGA_TSensors {
			i2cctrl.AddDevice(key, uint16(dev.address), dev.bus)
		}
		for key, dev := range LTM4700_TSensors {
			i2cctrl.AddDevice(key, uint16(dev.address), dev.bus)
		}
	}

	GCMTC := GCMThreadController{}

	http.HandleFunc("/monitor_voltages", i2cctrl.ReadINA226s)
	http.HandleFunc("/monitor_temperatures", i2cctrl.ReadTemperatures)
	http.HandleFunc("/start_thread", GCMTC.StartThread)
	http.HandleFunc("/stop_thread", GCMTC.StopThread)
	http.HandleFunc("/onlinemonitoring", i2cctrl.plotHandle)
	http.Handle("/data", websocket.Handler(dataHandler))

	fmt.Println("Server started at port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
	done <- true
}

func (ctrl I2CController) ReadINA226s(w http.ResponseWriter, req *http.Request) {

	results := make(map[string]float32)

	for name := range ctrl.INA226s {
		var data []byte
		if !ctrl.dryrun {
			ctrl.Write(name, []byte{0x2})
			data, _ = ctrl.Read(name, 2)
		} else {
			data = []byte{byte(rand.Intn(255)), byte(rand.Intn(255))}
		}
		value := uint16(data[0])<<8 + uint16(data[1])
		voltage := float32(value) * 1.25 / 1000.
		log.Printf("%v : %v V", name, voltage)
		results[name] = voltage
	}

	output, err := json.Marshal(results)
	if err != nil {
		log.Println(err)
	}

	fmt.Fprintln(w, string(output))
}

func (ctrl *I2CController) ReadTemperatures(w http.ResponseWriter, req *http.Request) {

	results := ctrl.GetTemperatures()

	output, err := json.Marshal(results)
	if err != nil {
		log.Println(err)
	}

	fmt.Fprintln(w, string(output))
}

// def lin5_11ToFloat(wordValue):
//   binValue = int(wordValue,16)
//   binValue=binValue>>8 | (binValue << 8 & 0xFF00)
//   #wordValue = ' '.join(format(x, 'b') for x in bytearray(wordValue))
//   exponent = binValue>>11      #extract exponent as MS 5 bits
//   mantissa = binValue & 0x7ff  #extract mantissa as LS 11 bits

//   #sign extended exponent
//   if exponent > 0x0F: exponent |= 0xE0
//   if exponent > 127: exponent -= 2**8
//   #sign extended mantissa
//   if mantissa > 0x03FF: mantissa |= 0xF800
//   if mantissa > 32768: mantissa -= 2**16
//   # compute value as
//   return mantissa * (2**exponent)

func pow2(exp int) uint32 {
	return 1 << exp
}

func lin5_11ToFloat(value uint32) float64 {
	binValue := value
	//binValue = (binValue >> 8) | ((binValue << 8) & 0xFF00)
	exponent := int((binValue >> 11) & 0x1F) //extract exponent as MS 5 bits
	mantissa := binValue & 0x7ff             //extract mantissa as LS 11 bits

	//sign extended exponent
	if exponent > 0x0F {
		exponent |= 0xE0
	}
	if exponent > 127 {
		exponent -= int(pow2(8))
	}
	//sign extended mantissa
	if mantissa > 0x03FF {
		mantissa |= 0xF800
	}
	if mantissa > 32768 {
		mantissa -= pow2(16)
	}
	// compute value as
	var final_value float64 = float64(mantissa) * math.Pow(2, float64(exponent))
	return final_value

}

func (ctrl *GCMThreadController) StartThread(w http.ResponseWriter, req *http.Request) {
	log.Println("starting thread")
	ctrl.stop = make(chan bool)
	go func(stop *chan bool) {
		for {
			log.Println("inloop")
			select {
			case <-ctrl.stop:
				log.Println("Stopping thread")
				return
			default:
				log.Println(time.Now())
				time.Sleep(500 * time.Millisecond)
			}
		}
	}(&ctrl.stop)
}

func (ctrl *GCMThreadController) StopThread(w http.ResponseWriter, req *http.Request) {
	ctrl.stop <- true
}
