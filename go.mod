module gitlab.cern.ch/bnl-omega-go/godaq2socforgcm

go 1.17

require (
	gitlab.cern.ch/bnl-omega-go/slowcontrol v0.0.0-20220323162737-bd25ae8c1474
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f
	gonum.org/v1/plot v0.10.0
)

require (
	github.com/ajstarks/svgo v0.0.0-20210923152817-c3b6e2f0c527 // indirect
	github.com/codehardt/mmap-go v1.0.1 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/go-fonts/liberation v0.2.0 // indirect
	github.com/go-latex/latex v0.0.0-20210823091927-c0d11ff05a81 // indirect
	github.com/go-pdf/fpdf v0.5.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	gitlab.cern.ch/bnl-omega-go/axi v0.0.0-20220219040116-cbda0ec4a509 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	periph.io/x/conn/v3 v3.6.10 // indirect
	periph.io/x/host/v3 v3.7.2 // indirect
)
